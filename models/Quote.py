from lib.vendor.peewee import *
from data.ogma_model import OgmaModel


class Quote(OgmaModel):
    quote = CharField()

    class Meta:
        table_name = 'quotes'
