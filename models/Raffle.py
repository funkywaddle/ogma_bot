from lib.vendor.peewee import *
from data.ogma_model import OgmaModel


class Raffle(OgmaModel):
    username = CharField()

    class Meta:
        table_name = 'raffles'
