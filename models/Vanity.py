from lib.vendor.peewee import *
from data.ogma_model import OgmaModel


class Vanity(OgmaModel):
    username = CharField(unique=True)
    response = TextField()

    class Meta:
        table_name = 'vanities'
