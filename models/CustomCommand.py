from lib.vendor.peewee import *
from data.ogma_model import OgmaModel


class CustomCommand(OgmaModel):
    command = CharField(unique=True)
    response = CharField()
    activated = BooleanField()

    class Meta:
        table_name = 'custom_commands'
