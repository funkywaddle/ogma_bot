from lib.vendor.peewee import *
from data.ogma_model import OgmaModel


class AudioHook(OgmaModel):
    command = CharField()
    file = CharField()
    activated = BooleanField()

    class Meta:
        table_name = 'audio_hooks'
