from lib.vendor.peewee import *
from data.ogma_model import OgmaModel


class LoyaltyPoint(OgmaModel):
    username = CharField()
    points = IntegerField()

    class Meta:
        table_name = 'loyalty_points'
