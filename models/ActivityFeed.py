from lib.vendor.peewee import *
from data.ogma_model import OgmaModel


class ActivityFeed(OgmaModel):
    username = CharField()
    activity = CharField()
    message = TextField(null=True)
    amount = DecimalField(null=True)

    class Meta:
        table_name = 'activity_feed'
