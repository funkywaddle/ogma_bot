from lib.vendor.peewee import *
from data.ogma_model import OgmaModel


class TimedChat(OgmaModel):
    response = CharField()

    class Meta:
        table_name = 'timed_chats'
