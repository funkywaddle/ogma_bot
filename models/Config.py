from lib.vendor.peewee import *
from data.ogma_model import OgmaModel


class Config(OgmaModel):
    variable = CharField()
    setting = CharField()

    class Meta:
        table_name = 'twitch_configs'

    @classmethod
    def get_config(cls, key):
        cfg = cls.get(cls.variable == key)
        return cfg.setting
