from lib.vendor.peewee import *
from data.ogma_model import OgmaModel


class Quip(OgmaModel):
    trigger = CharField(unique=True)
    response = CharField()

    class Meta:
        table_name = 'quips'
