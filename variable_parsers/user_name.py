import re
from lib.Response import Response
from lib.VariableParser import parser, VariableParser


@parser()
class UserName(VariableParser):
    def __init__(self):
        super().__init__()
        self.variable = 'user.name'
        self.pattern = f'{self.var_open}{self.variable}\|(\d+){self.var_close}'

    def parse(self, response: Response):
        msg_parts = response.message.message.split()
        cmd = msg_parts[0][1:]
        parameters = msg_parts[1:]
        match = re.search(self.pattern, response.text)

        if match is not None:
            parameter_num = int(match.group(1))
            found = f'{self.var_open}{self.variable}|{parameter_num}{self.var_close}'
            parameter_num -= 1

            if len(parameters) > parameter_num:
                try:
                    user = int(parameters[parameter_num])
                    response.text = VariableParser.get_wrong_parameters_text(parameters[0])
                except ValueError:
                    user = parameters[parameter_num]

                    if user.startswith('@'):
                        user = user[1:]
                    response.text = response.text.replace(found, user)
            else:
                response.text = VariableParser.get_incomplete_parameters_text(cmd)

        return response
