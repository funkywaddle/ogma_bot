# import re
# from pprint import pprint
#
# import requests
#
# from ..libs.parser import Parser
# from ..models.Config import Config
#
#
# class UserGame(Parser):
#     def __init__(self):
#         super().__init__()
#         self.variable = 'user.game'
#         self.pattern = f'{self.var_open}{self.variable}\|(\d+){self.var_close}'
#
#     def parse(self, response: Response):
#         parameters = ctx.parameters
#         match = re.search(self.pattern, text)
#
#         if match is not None:
#             parameter_num = int(match.group(1))
#             found = f'{self.var_open}{self.variable}|{parameter_num}{self.var_close}'
#             parameter_num -= 1
#             client_id = Config.get_config('CLIENT_ID')
#             user = parameters[parameter_num]
#
#             if user.startswith('@'):
#                 user = user[1:]
#
#             base_url = 'https://api.twitch.tv/kraken'
#             user_url = f'{base_url}/users?login={user}'
#
#             headers = {'Client-ID': client_id, 'Accept': 'application/vnd.twitchtv.v5+json'}
#             r = requests.get(user_url, headers=headers).json()
#             channel_url = f'{base_url}/channels/{r["users"][0]["_id"]}'
#             r = requests.get(channel_url, headers=headers).json()
#             if r['status'] is not None:
#                 text = text.replace(found, r['status'])
#             else:
#                 text = text.replace(found, 'NONE')
#         return text
