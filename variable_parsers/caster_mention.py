import re
from config import CASTER
from lib.Response import Response
from lib.VariableParser import parser, VariableParser


@parser()
class CasterMention(VariableParser):
    def __init__(self):
        super().__init__()
        self.variable = 'caster.mention'
        self.pattern = f'{self.var_open}{self.variable}{self.var_close}'

    def parse(self, response: Response):
        match = re.search(self.pattern, response.text)
        username = CASTER

        if match is not None:
            found = f'{self.var_open}{self.variable}{self.var_close}'

            if not username.startswith('@'):
                username = f'@{username}'
            response.text = response.text.replace(found, username)

        return response
