import sys
from lib.AudioCommand import command


@command()
class Exit:
    def __init__(self):
        pass

    def run(self):
        sys.exit()