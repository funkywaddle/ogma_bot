from lib.Message import Message
from lib.Commands import command
from lib.helpers import get_directory_contents
import importlib
from pprint import pprint


def adder(name: str = None):
    def wrapper(klass):
        class Adder(klass):
            def __new__(cls, *args, **kwargs):
                result = super().__new__(klass)
                result.__init__()
                return result
        adr_name = name or klass.__name__

        Add.adders[adr_name.lower()] = Adder()
        return Adder
    return wrapper


@command()
class Add:
    adders = {}

    def __init__(self):
        self.adders = {}
        adders = get_directory_contents('commands/adders')
        for adr in adders:
            path = f'commands.adders.{adr}'
            adr_module = importlib.import_module(path)
            adr_class = getattr(adr_module, adr.title())
            self.adders[adr] = adr_class()
        pprint(self.adders)

    def run(self, msg: Message = None, params=None):
        cmd = params[0]
        parameters = params[1:]
        if cmd in self.adders.keys():
            return self.adders[cmd].add(msg, parameters)
