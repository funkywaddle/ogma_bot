from lib.Message import Message
from lib.Commands import command
from lib.helpers import get_directory_contents
import importlib
from pprint import pprint


def deleter(name: str = None):
    def wrapper(klass):
        class Deleter(klass):
            def __new__(cls, *args, **kwargs):
                result = super().__new__(klass)
                result.__init__()
                return result
        del_name = name or klass.__name__

        Delete.deleters[del_name.lower()] = Deleter()
        return Deleter
    return wrapper


@command()
class Delete:
    deleters = {}

    def __init__(self):
        self.deleters = {}
        deleters = get_directory_contents('commands/deleters')
        for dlt in deleters:
            path = f'commands.deleters.{dlt}'
            del_module = importlib.import_module(path)
            del_class = getattr(del_module, dlt.title())
            self.deleters[dlt] = del_class()
        pprint(self.deleters)

    def run(self, msg: Message = None, params=None):
        cmd = params[0]
        parameters = params[1:]
        if cmd in self.deleters.keys():
            return self.deleters[cmd].delete(msg, parameters)
