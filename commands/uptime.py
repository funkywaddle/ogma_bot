import requests
from datetime import datetime, timezone
from lib.Message import Message
from lib.Commands import command
from lib.Twitch.Api import Api
from config import CHAN, CHAN_ID, CLIENT_ID


@command()
class Uptime:

    def run(self, msg: Message = None, params=None):
        api = Api()
        r = api.stream(CHAN_ID)
        utc_date = datetime.strptime(r['data'][0]['started_at'], "%Y-%m-%dT%H:%M:%SZ")
        started_at = utc_date.replace(tzinfo=timezone.utc).astimezone(tz=None)
        today = datetime.now().astimezone(tz=None)

        minutes = 0
        hours = 0

        uptime = today - started_at
        if uptime.days == 0:
            cycle = 'seconds'
            num = int(uptime.seconds)
            if uptime.seconds > 60:
                minutes = uptime.seconds / 60
                cycle = 'minutes'
                num = int(minutes)
                if minutes > 60:
                    hours = int(minutes / 60)
                    minutes = int(minutes % 60)
                    num = f'{hours} hours, {minutes}'
        else:
            num = uptime.days
            cycle = 'day' if uptime.days == 1 else 'days'
        output = f'{num} {cycle}'

        response = f"{r['data'][0]['user_name']} has been streaming for {output}"
        return "{{caller.mention}}, " + response
