from lib.Commands import requires_level, Levels
from lib.Message import Message
from models.Quote import Quote as QuoteMdl


class Quote:
    def __init__(self):
        pass

    @requires_level(Levels.Subscriber)
    def add(self, e: Message, args):
        response = ' '.join(args)

        data = {
            'quote': response
        }

        quote = QuoteMdl.create(**data)

        response = f'Quote {quote.id} has been successfully added'
        return '{{caller.mention}}, ' + response
