from lib.Commands import requires_level, Levels
from lib.Message import Message
from models.CustomCommand import CustomCommand, IntegrityError


class Command:
    def __init__(self):
        pass

    @requires_level(Levels.VIP)
    def add(self, e: Message, args):
        cmd_name = args.pop(0)
        response = ' '.join(args)

        data = {
            'command': cmd_name,
            'response': response,
            'activated': 1
        }

        try:
            CustomCommand.create(**data)
        except IntegrityError as e:
            return f'That command already exists. Please edit the command to change the response'

        return f'Command {cmd_name} has been successfully added'
