from lib.Commands import requires_level, Levels
from lib.Message import Message
from models.Quip import Quip as QuipMdl


class Quip:
    def __init__(self):
        pass

    @requires_level(Levels.Moderator)
    def add(self, e: Message, args):
        trigger_word = args.pop(0)
        response = ' '.join(args)

        data = {
            'trigger': trigger_word,
            'response': response
        }

        try:
            QuipMdl.create(**data)
            response = f'Quip for {trigger_word} has been successfully added'
        except Exception as e:
            response = f'A quip with trigger {trigger_word} was already present. Please use the !edit command to edit the response'

        return '{{caller.mention}}, ' + response
