import requests
from lib.Message import Message
from lib.Twitch.Api import Api
from config import CHAN, CHAN_ID, CLIENT_ID
from lib.Commands import command


@command()
class Game:

    def run(self, msg: Message = None, params=None):
        api = Api()
        r = api.channel(CHAN_ID)
        r = r['data'][0]
        response = f"{r['broadcaster_name']} is currently in the {r['game_name']} category"
        return response
