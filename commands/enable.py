from lib.Message import Message
from lib.Commands import command
from lib.helpers import get_directory_contents
import importlib
from pprint import pprint


def enabler(name: str = None):
    def wrapper(klass):
        class Enabler(klass):
            def __new__(cls, *args, **kwargs):
                result = super().__new__(klass)
                result.__init__()
                return result
        enabler_name = name or klass.__name__

        Enable.enablers[enabler_name.lower()] = Enabler()
        return Enabler
    return wrapper


@command()
class Enable:
    enablers = {}

    def __init__(self):
        self.enablers = {}
        enablers = get_directory_contents('commands/enablers')
        for enabler_cmd in enablers:
            path = f'commands.enablers.{enabler_cmd}'
            enabler_module = importlib.import_module(path)
            enabler_class = getattr(enabler_module, enabler_cmd.title())
            self.enablers[enabler_cmd] = enabler_class()
        pprint(self.enablers)

    def run(self, msg: Message = None, params=None):
        cmd = params[0]
        parameters = params[1:]
        if cmd in self.enablers.keys():
            return self.enablers[cmd].enable(msg, parameters)
