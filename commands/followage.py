from datetime import datetime, timezone
from lib.Message import Message
from lib.Commands import command


@command()
class Followage:

    def run(self, msg: Message = None, params=None):
        user = msg.user

        if user.is_follower:
            utc_date = user.date_followed
            date_followed = utc_date.replace(tzinfo=timezone.utc).astimezone(tz=None)
            today = datetime.now().astimezone(tz=None)
            minutes = 0
            hours = 0

            days_followed = today - date_followed
            if days_followed.days == 0:
                cycle = 'seconds'
                num = int(days_followed.seconds)
                if days_followed.seconds > 60:
                    minutes = days_followed.seconds / 60
                    cycle = 'minutes'
                    num = int(minutes)
                    if minutes > 60:
                        hours = int(minutes / 60)
                        minutes = int(minutes % 60)
                        num = f'{hours} hours, {minutes}'
            else:
                num = days_followed.days
                cycle = 'day' if days_followed.days == 1 else 'days'
            output = f'{num} {cycle} ago'

            response = f"you have followed since {date_followed.strftime('%B %d, %Y')} ({output})"
        else:
            response = f"you are not following this channel."
        return "{{caller.mention}}, "+response
