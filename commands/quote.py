from lib.vendor.peewee import *
from lib.Message import Message
from lib.Commands import command
from models.Quote import Quote as QuoteMdl

@command()
class Quote:

    def run(self, msg: Message = None, params=None):
        try:
            qid = int(params[0])
            quote = QuoteMdl.select().where(QuoteMdl.id == qid).get()
            message = quote.quote
        except DoesNotExist as e:
            message = "{{caller.mention}}, a quote could not be found with that id"
        except ValueError as e:
            try:
                quote = QuoteMdl.select().where(QuoteMdl.quote.contains(params[0])).get()
                message = quote.quote
            except DoesNotExist as e:
                message = "{{caller.mention}}, a quote could not be found with that term"

        return message

