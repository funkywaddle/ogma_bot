from lib.Message import Message
from lib.Commands import command
import random


@command(name='8ball')
class Eightball:

    def __init__(self):
        self.responses = self.get_responses()

    def run(self, msg: Message = None, params=None):
        if len(params) == 0:
            return 'You need to ask a question, silly.'
        return random.choice(self.responses)

    def get_responses(self):
        responses = [
            'Heck NO!',
            'How would I know?',
            'Sure, why not?',
            'It is decidedly so',
            'Outlook not good (Neither is Word).',
            'No, no, no, no, no',
            'YES!!!',
            'Could be',
            'Probably not',
            'Do I look like I know?',
            'Ummmmmmmm, no',
            'How bout NO',
            'YEAH BOY!!!!',
            'Yessir',
            'Yeah, sure, right, uh huh.',
            'You got it, dude!',
            'VoteYea',
            'VoteNay',
            'Most Likely',
            'Does not compute'
        ]
        return responses
