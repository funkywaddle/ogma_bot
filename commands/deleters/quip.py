from lib.Commands import requires_level, Levels
from lib.Message import Message
from models.Quip import Quip as QuipMdl


class Quip:
    def __init__(self):
        pass

    @requires_level(Levels.Moderator)
    def delete(self, e: Message, args):
        trigger_word = args.pop(0)

        try:
            quip = QuipMdl.get(QuipMdl.trigger == trigger_word)
            quip.delete_instance()
            response = f'Quip for "{trigger_word}" has been successfully deleted'
        except Exception as e:
            response = f'A quip with trigger "{trigger_word}" could not be found'

        return '{{caller.mention}}, ' + response
