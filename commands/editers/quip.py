from lib.vendor.peewee import *
from lib.Commands import requires_level, Levels
from lib.Message import Message
from models.Quip import Quip as QuipMdl


class Quip:
    def __init__(self):
        pass

    @requires_level(Levels.Moderator)
    def edit(self, e: Message, args):
        trigger_word = args.pop(0)
        response = ' '.join(args)

        try:
            quip = QuipMdl.get(QuipMdl.trigger == trigger_word)
            quip.response = response
            quip.save()
            response = f'Quip for "{trigger_word}" has been successfully edited'
        except DoesNotExist as e:
            response = f'No Quip found for "{trigger_word}"'

        return response
