from lib.vendor.peewee import *
from lib.Commands import requires_level, Levels
from lib.Message import Message
from models.CustomCommand import CustomCommand


class Command:
    def __init__(self):
        pass

    @requires_level(Levels.VIP)
    def edit(self, e: Message, args):
        cmd_name = args.pop(0)
        response = ' '.join(args)

        try:
            command = CustomCommand.get(CustomCommand.command == cmd_name)
            command.response = response
            command.save()
            response = f'Command "{cmd_name}" has been successfully edited'
        except DoesNotExist as e:
            response = f'No Command found for "{cmd_name}"'

        return response
