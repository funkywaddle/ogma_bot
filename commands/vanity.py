from lib.vendor.peewee import *
from models.Vanity import Vanity as VanityModel
from lib.Message import Message
from lib.Commands import command, requires_level, Levels


@command()
class Vanity:

    @requires_level(Levels.VIP)
    def run(self, msg: Message = None, params=None):
        username = msg.username
        response = ' '.join(params)

        try:
            vanity_query = VanityModel.select().\
                where(
                    fn.Lower(VanityModel.username) == fn.Lower(username)
                )
            vanity_exists = vanity_query.exists()

            if vanity_exists:
                vanity = vanity_query.get()
                vanity.response = response
                vanity.save()

                return_text = "updated"
            else:
                data = {
                    "username": username,
                    "response": response
                }

                VanityModel.create(**data)

                return_text = "added"

            return_text = f"your vanity response has been successfully {return_text}"
            return "{{caller.mention}}, " + return_text
        except DoesNotExist:
            return "{{caller.name}}, there is no command by that name."
