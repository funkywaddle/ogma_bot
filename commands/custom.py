from lib.vendor.peewee import *
from models.CustomCommand import CustomCommand
from lib.Message import Message
from lib.Commands import command


@command()
class Custom:

    def run(self, msg: Message = None, params=None):
        cmd = params[0]

        try:
            command = CustomCommand.select().\
                where(
                    (fn.Lower(CustomCommand.command) == fn.Lower(cmd))
                    & (CustomCommand.activated == 1)
                ).get()
            return command.response
        except DoesNotExist:
            return "{{caller.name}}, there is no command by that name."
