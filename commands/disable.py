from lib.Message import Message
from lib.Commands import command
from lib.helpers import get_directory_contents
import importlib
from pprint import pprint


def disabler(name: str = None):
    def wrapper(klass):
        class Disabler(klass):
            def __new__(cls, *args, **kwargs):
                result = super().__new__(klass)
                result.__init__()
                return result
        disabler_name = name or klass.__name__

        Disable.disablers[disabler_name.lower()] = Disabler()
        return Disabler
    return wrapper


@command()
class Disable:
    disablers = {}

    def __init__(self):
        self.disablers = {}
        disablers = get_directory_contents('commands/disablers')
        for disabler_cmd in disablers:
            path = f'commands.disablers.{disabler_cmd}'
            disabler_module = importlib.import_module(path)
            disabler_class = getattr(disabler_module, disabler_cmd.title())
            self.disablers[disabler_cmd] = disabler_class()
        pprint(self.disablers)

    def run(self, msg: Message = None, params=None):
        cmd = params[0]
        parameters = params[1:]
        if cmd in self.disablers.keys():
            return self.disablers[cmd].disable(msg, parameters)
