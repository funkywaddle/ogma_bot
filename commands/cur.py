from pprint import pprint
import requests
from lib.Message import Message
from lib.Commands import command


@command()
class Cur:

    def run(self, msg: Message = None, params=None):
        symbol = params[0].upper() if len(params) > 0 else 'BTC'
        base = params[1].upper() if len(params) > 1 else 'USD'

        url = f'https://api.coinranking.com/v1/public/coins?symbols={symbol}&base={base}'
        r = requests.get(url).json()
        data = r['data'] if 'data' in r else None
        if data is not None:
            coin = data['coins'][0] if data['coins'][0] is not None else {"name": symbol, "price": "NO. Just No"}
            base = data['base']
            base['sign'] = base['sign'] if base['sign'] is not None else ''
            response = f'{coin["name"]} is currently trading for {base["sign"]}{coin["price"]} {base["symbol"]}'
        else:
            response = f'Sorry, something went wrong. Please try again.'
        return response
