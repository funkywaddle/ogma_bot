from lib.Message import Message
from lib.Commands import command
from lib.helpers import get_directory_contents
import importlib
from pprint import pprint


def editer(name: str = None):
    def wrapper(klass):
        class Editer(klass):
            def __new__(cls, *args, **kwargs):
                result = super().__new__(klass)
                result.__init__()
                return result
        edtr_name = name or klass.__name__

        Edit.editers[edtr_name.lower()] = Editer()
        return Editer
    return wrapper


@command()
class Edit:
    editers = {}

    def __init__(self):
        self.editers = {}
        editers = get_directory_contents('commands/editers')
        for edtr in editers:
            path = f'commands.editers.{edtr}'
            edtr_module = importlib.import_module(path)
            edtr_class = getattr(edtr_module, edtr.title())
            self.editers[edtr] = edtr_class()
        pprint(self.editers)

    def run(self, msg: Message = None, params=None):
        cmd = params[0]
        parameters = params[1:]
        if cmd in self.editers.keys():
            return self.editers[cmd].edit(msg, parameters)
