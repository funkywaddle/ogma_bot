from lib.Commands import requires_level, Levels
from lib.Message import Message
from models.CustomCommand import CustomCommand


class Command:
    def __init__(self):
        pass

    @requires_level(Levels.Moderator)
    def enable(self, e: Message, args):
        cmd_name = args.pop(0)

        command = CustomCommand.get(CustomCommand.command == cmd_name)
        command.activated = 1
        command.save()

        return f'Command {cmd_name} has been successfully enabled'
