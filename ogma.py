import concurrent.futures

from lib.Quip import Quip
from lib.Transcriber import Transcriber
from lib.Twitch.Chat import Chat
from lib.Twitch.Api import Api
from lib.Twitch.Auth import Auth
from lib.helpers import get_directory_contents
from lib.Response import Response
from lib.Vanities import Vanities
from pprint import pprint
from config import *
from time import sleep
from lib.Message import Message
from lib.User import User
from lib.Commands import Commands
from lib.VariableParser import VariableParser
from lib.AudioCommand import AudioCommand
from lib.Twitch.Alerts import Alerts
import lib.vendor.speech_recognition as sr
import importlib
import sys


class Ogma:

    def __init__(self):
        # TODO: install if first run
        self.chat = None
        self.alerts = None
        self.api = Api()
        self.auth = Auth()
        self.users = {}
        self.auth.get_bearer_token()
        self.voice = sr.Recognizer()
        self.listen_for_commands = True
        self.audio_commands = AudioCommand()
        self.chat_thread = None
        self.alert_thread = None
        self.voice_thread = None

    def start(self, chat_cnct=None, alert_cnct=None):
        if chat_cnct is None:
            chat_cnct = Chat()
        # if alert_cnct is None:
        #     alert_cnct = Alerts()
        self.prep_commands()
        self.prep_parsers()
        self.prep_audio_commands()
        self.start_chat(chat_cnct)
        # self.start_alerts(alert_cnct)

    def prep_commands(self):
        commands = get_directory_contents('commands')
        for cmd in commands:
            path = f'commands.{cmd}'
            importlib.import_module(path)

    def prep_parsers(self):
        parsers = get_directory_contents('variable_parsers')
        for psr in parsers:
            path = f'variable_parsers.{psr}'
            importlib.import_module(path)

    def prep_audio_commands(self):
        audio_commands = get_directory_contents('audio_commands')
        for cmd in audio_commands:
            path = f'audio_commands.{cmd}'
            importlib.import_module(path)

    def start_chat(self, chat_connection):
        self.chat = chat_connection
        self.chat.start()
        self.chat.login(PASS, NICK, CHAN)
        self.chat.set_capabilities('membership', 'tags', 'commands')

    def start_alerts(self, alert):
        self.alerts = alert
        self.alerts.add_listener(f'channel-bits-events-v1.{CHAN_ID}')
        self.alerts.add_listener(f'channel-points-channel-v1.{CHAN_ID}')
        self.alerts.add_listener(f'channel-subscribe-events-v1.{CHAN_ID}')
        self.alerts.add_listener(f'chat_moderator_actions.{CHAN_ID}')
        print(self.alerts.send_listen_requests())
        # pass

    def process_alerts(self):
        while self.chat.connected:
            sleep(30)
            # self.chat.send_chat('Hola')

    def process_message(self, msg):
        twitchuser = self.api.user(msg.username)
        usr = User(twitchuser)
        follows = self.api.user_follows(usr)
        usr.follows(follows)
        self.users[msg.username] = usr
        msg.user = usr

    def process_vanities(self, msg):
        # if user has custom command, run it
        vanities = Vanities()
        vanity_response = vanities.process(msg)
        if vanity_response is not None:
            self.chat.send_chat(vanity_response)

    def process_chat(self):
        while self.chat.connected:
            response = self.chat.get_message()
            if response is not None:
                pprint(response)
                msg = Message(response)
                if msg.username is not None:
                    if msg.username not in self.users.keys():
                        self.process_message(msg)
                        self.process_vanities(msg)
                    else:
                        msg.user = self.users[msg.username]
                    # pass

                    cmds = Commands()
                    if cmds.isCommand(msg):
                        cmd_response = cmds.parse(msg)
                        self.process_response(cmd_response)
                    else:
                        transcriber = Transcriber()
                        cmd_response = transcriber.translate(msg)
                        if cmd_response is None:
                            cmd_response = Quip().quip_it(msg)

                    if cmd_response is not None and cmd_response.text is not None:
                        self.chat.send_chat(cmd_response.text)
                    print(msg.user.display_name + ": " + msg.message)
            sleep(1 / RATE)

    def process_voice(self):
        while self.listen_for_commands:
            self.listen()

    def process_audio_text(self, audio_text):
        if 'bot'.lower() in audio_text.lower():
            self.audio_commands.initiated = True
        elif self.audio_commands.initiated == True:
            self.audio_commands.parse(audio_text)

    def listen(self):
        with sr.Microphone() as source:
            audio = self.voice.listen(source)
            audio_text = self.voice.recognize_google(audio)
            self.process_audio_text(audio_text)

    def process_response(self, res: Response):
        if res.text is not None:
            VariableParser().process(res)


if __name__ == "__main__":
    bot = Ogma()
    bot.start(Chat())
    with concurrent.futures.ThreadPoolExecutor() as executor:
        bot.chat_thread = executor.submit(bot.process_chat)
        # bot.voice_thread = executor.submit(bot.process_voice)
        # bot.alert_thread = executor.submit(bot.process_alerts)
