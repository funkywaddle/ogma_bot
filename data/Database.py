from lib.vendor.peewee import SqliteDatabase


class Database:
    db = SqliteDatabase('data/ogma.data')

    @classmethod
    def get_database(cls):
        return cls.db
