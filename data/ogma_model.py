from lib.vendor.peewee import *
from data.Database import Database


class OgmaModel(Model):
    class Meta:
        database = Database.get_database()
