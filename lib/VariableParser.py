from lib.Response import Response


def parser(name: str = None):
    def wrapper(klass):
        class Parser(klass):
            def __new__(cls, *args, **kwargs):
                result = super().__new__(cls)

                result.__module__ = klass.__module__
                result.__name__ = name or klass.__name__.lower()

                return result
        parser_name = name or klass.__name__.lower()
        VariableParser.parsers[parser_name] = Parser()
        return Parser
    return wrapper


class VariableParser:
    parsers = {}

    def __init__(self):
        self.var_open = '{{'
        self.var_close = '}}'

    def process(self, response: Response):
        for psr in VariableParser.parsers:
            VariableParser.parsers[psr].parse(response)
        return response

    @classmethod
    def get_wrong_parameters_text(cls, variable):
        return f'Your parameters for "{variable}" are incorrect'

    @classmethod
    def get_incomplete_parameters_text(cls, variable):
        return f'You did not provide enough parameters for "{variable}"'
