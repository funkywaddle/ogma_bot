def get_directory_contents(directory):
    from os.path import basename, isfile
    import glob
    modules = glob.glob(f"{directory}/*.py")
    files = []
    for f in modules:
        if isfile(f) and not f.endswith('__init__.py') and not basename(f).startswith('_'):
            files.append(basename(f)[:-3])

    return files
