from pprint import pprint
import re
# from .googletrans import Translator
# from .quipper import Quipper
# from ..config import CHANNEL_LANGUAGE


class Message:
    def __init__(self, message=None):
        self.user = None
        self.username = None
        self.message = None
        self.tags = None
        self.orig_tags = None
        self.from_regex(message)

    def from_regex(self, msg: str):
        chat_msg_regex = r"(.*):(\w+)!\w+@\w+\.tmi\.twitch\.tv PRIVMSG #\w+ :(.*)\r\n"
        regex = re.search(chat_msg_regex, msg)
        if regex is not None:
            self.orig_tags = regex.group(1)
            self.tags = self.split_tags(regex.group(1))
            self.username = regex.group(2)
            self.message = regex.group(3)

    def split_tags(self, tags):
        tgs = tags.split(';')
        rtn = {}
        for tg in tgs:
            key, value = tg.split('=')
            key.strip()
            value.strip()
            key = key[1:] if key.startswith('@') else key
            value = None if value == '' else value
            rtn[key] = value
        return rtn

    # @classmethod
    # def get_parameters(cls, event):
    #     return event.arguments[0].split()[1:]

    # def process(self):
    #     tr_message = Translator().translate(self.message, dest=CHANNEL_LANGUAGE)
    #     if tr_message.text.strip() == self.message:
    #         quips = self.run_quips()
    #         if quips is not None:
    #             return quips
    #     else:
    #         return f'Translated: {tr_message.text}'

    # def run_quips(self):
    #     quip = Quipper().quip_it(self.message)
    #     if quip is not None:
    #         return quip
