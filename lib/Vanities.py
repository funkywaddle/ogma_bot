from models.Vanity import Vanity
from lib.vendor.peewee import *
from lib.Message import Message


class Vanities:
    def process(self, msg: Message = None):
        username = msg.username

        vanity_query = Vanity.select(). \
            where(
            fn.Lower(Vanity.username) == fn.Lower(username)
        )
        vanity_exists = vanity_query.exists()

        if vanity_exists:
            vanity = vanity_query.get()
            response = vanity.response

            return response
