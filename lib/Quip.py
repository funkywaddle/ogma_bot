from pprint import pprint

from .Message import Message
from .Response import Response
from models.Quip import Quip as QuipMdl
import re


class Quip:
    def __init__(self):
        self.quips = {}
        self.load_quips()

    def quip_it(self, msg: Message):
        text = msg.message

        response = None
        for quip in self.quips.keys():
            if self.find_whole_word(quip)(text):
                response = Response()
                response.message = msg
                response.text = self.quips[quip]
                break

        return response

    @staticmethod
    def find_whole_word(w):
        return re.compile(r'\b({0})\b'.format(w), flags=re.IGNORECASE).search

    def load_quips(self):
        self.quips = {}
        quips = QuipMdl.select()

        for quip in quips:
            self.quips[quip.trigger] = quip.response
