from lib.Message import Message
from lib.Response import Response
import functools
import importlib
from pprint import pprint
from lib.helpers import get_directory_contents


class Levels:
    Follower = ['follower', 'founder', 'subscriber', 'vip', 'moderator', 'broadcaster']
    Subscriber = ['founder', 'subscriber', 'vip', 'moderator', 'broadcaster']
    VIP = ['vip', 'moderator', 'broadcaster']
    Moderator = ['moderator', 'broadcaster']
    Broadcaster = ['broadcaster']


def requires_level(level):
    def required_decorator(func):
        @functools.wraps(func)
        def func_wrapper(self, e: Message, args):
            badges = e.tags['badges']
            user_badges = badges.split(',') if badges is not None else []
            user_levels = ['follower'] if e.user.is_follower else []
            for lvl in user_badges:
                user_levels.append(lvl.split('/')[0])
            if len(list(set(user_levels) & set(level))) > 0:
                return func(self, e, args)
            else:
                return '{{caller.mention}}, you do not have sufficient access to call this command'
        return func_wrapper
    return required_decorator


def command(name: str = None):
    def wrapper(klass):
        class Command:
            def __new__(cls, *args, **kwargs):
                result = super().__new__(klass)
                result.__init__()
                return result
        cmd_name = name or klass.__name__
        Commands.commands[cmd_name.lower()] = Command()
        return Command
    return wrapper


class Commands:
    commands = {}

    def __init__(self):
        pass

    def parse(self, msg: Message):
        msg_parts = msg.message.split()

        cmd_response = None
        if msg_parts[0][0] == '!':
            cmd = msg_parts[0][1:].lower()
            parameters = msg_parts[1:]

            if cmd in Commands.commands.keys():
                cmd_response = Commands.commands[cmd].run(msg, parameters)
            else:
                cmd_response = Commands.commands['custom'].run(msg, [cmd])

        res = Response()
        res.message = msg
        res.text = cmd_response
        return res

    def isCommand(self, msg):
        msg_parts = msg.message.split()
        return msg_parts[0][0] == '!'
