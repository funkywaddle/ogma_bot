from datetime import datetime

class User:
    def __init__(self, userdict):
        self.id = None
        self.login = None
        self.display_name = None
        self.type = None
        self.broadcaster_type = None
        self.description = None
        self.profile_image_url = None
        self.offline_image_url = None
        self.process_user_dict(userdict)
        self.is_follower = False
        self.date_followed = None

    def process_user_dict(self, userdict):
        self.id = userdict.id
        self.login = userdict.login
        self.display_name = userdict.display_name
        self.type = userdict.type
        self.broadcaster_type = userdict.broadcaster_type
        self.description = userdict.description
        self.profile_image_url = userdict.profile_image_url
        self.offline_image_url = userdict.offline_image_url

    def follows(self, follows):
        if follows['total'] == 1:
            self.is_follower = True
            self.date_followed = datetime.strptime(follows['data'][0]['followed_at'], '%Y-%m-%dT%H:%M:%SZ')
