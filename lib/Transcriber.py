from lib.Message import Message
from lib.Response import Response
from lib.vendor.googletrans import Translator
from config import CHANNEL_LANGUAGE


class Transcriber:

    def __init__(self):
        self.translator = Translator()

    def translate(self, msg: Message):
        translated = self.translator.translate(msg.message, dest=CHANNEL_LANGUAGE)
        if translated.text != msg.message:
            res = Response()
            res.message = msg
            res.text = f'Translated:  {translated.text}'

            return res
