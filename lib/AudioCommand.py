def command(name: str = None):
    def wrapper(klass):
        class Command:
            def __new__(cls, *args, **kwargs):
                result = super().__new__(klass)
                result.__init__()
                return result
        cmd_name = name or klass.__name__
        AudioCommand.commands[cmd_name.lower()] = Command()
        return Command
    return wrapper


class AudioCommand:
    commands = {}

    def __init__(self):
        self.initiated = False

    def parse(self, audio_text):
        for audio_cmd in AudioCommand.commands.keys():
            if audio_text in audio_cmd:
                AudioCommand.commands[audio_text].run()
        self.initiated = False
