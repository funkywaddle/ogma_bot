from config import CLIENT_ID, CLIENT_SECRET
import requests
import json


class Auth:
    _instance = None
    bearer_token = None
    access_token = None

    def __new__(cls):
        if cls._instance is None:
            print('Creating the auth object')
            cls._instance = super(Auth, cls).__new__(cls)
            # Put any initialization here.
            Auth.bearer_token = 'Bearer ' + Auth.get_bearer_token()
            # Auth.access_token = Auth.get_access_token()
        return cls._instance

    @classmethod
    def get_bearer_token(cls):
        base_url = "https://id.twitch.tv/oauth2/token"
        params = dict(client_id=CLIENT_ID, client_secret=CLIENT_SECRET, grant_type='client_credentials', scope=None)
        scopes = ' '.join(Auth.get_scopes())
        params['scope'] = scopes
        response = requests.post(base_url, params)
        token = json.loads(response.content)
        return token['access_token']

    @classmethod
    def get_access_token(cls):
        base_url = "https://id.twitch.tv/oauth2/authorize"
        params = {
            'response_type': 'code',
            'client_id': CLIENT_ID,
            'redirect_uri': 'http://localhost',
            'scope': " ".join(Auth.get_scopes()),
            'state': 'c3ab8aa609ea11e793ae92361f002642'
        }

        import requests
        webauth = requests.get(base_url, params=params)
        code = webauth


    @classmethod
    def get_scopes(cls):
        return [
            'bits:read',
            'channel:read:subscriptions',
            'user:edit',
            'user:edit:broadcast',
            'user:edit:follows',
            'user:read:broadcast',
            'user:read:email',
            'channel:moderate',
            'chat:edit',
            'chat:read',
            'whispers:read',
            'whispers:edit'
        ]
