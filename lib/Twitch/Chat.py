from socket import socket


class Chat(socket):
    def __init__(self):
        super().__init__()
        self.connected = False
        self.host = "irc.twitch.tv"
        self.port = 6667
        self.channel = None

    def start(self):
        try:
            self.connect((self.host, self.port))
            self.connected = True
        except Exception as e:
            print(str(e))

    def end(self):
        self.close()

    def login(self, pwd, nick, chan):
        self.send_message(f"PASS oauth:{pwd}")
        self.send_message(f"NICK {nick}")
        self.send_message(f"JOIN {chan}")
        self.channel = chan

    def send_chat(self, msg):
        if msg is not None:
            self.send_message(f"PRIVMSG {self.channel} :{msg}")

    def send_message(self, msg):
        # print(f'sending: {msg}')
        self.send(f"{msg}\r\n".encode("utf-8"))

    def set_capabilities(self, *args):
        for cap in args:
            self.send_message(f"CAP REQ :twitch.tv/{cap}")

    def get_message(self):
        try:
            self.fileno()
        except Exception:
            self.start()
        msg = self.recv(1024).decode("utf-8")
        pinged = self.check_ping_pong(msg)
        if pinged:
            return None
        else:
            return msg

    def check_ping_pong(self, response):
        ping = False
        if response == "PING :tmi.twitch.tv\r\n":
            print('PING initiated')
            self.send_message("PONG :tmi.twitch.tv")
            print("PONG sent")
            ping = True
        return ping
