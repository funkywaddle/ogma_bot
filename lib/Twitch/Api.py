import lib.vendor.twitch as twitch
from lib.Twitch.Auth import Auth
from config import CLIENT_ID, CLIENT_SECRET, CHAN_ID
from lib.User import User
import requests
import json


class Api:

    def __init__(self):
        self.auth = Auth()
        self.bearer_token = 'Bearer ' + self.auth.get_bearer_token().lower().lstrip('bearer').strip()
        self.twitch = twitch.Helix(
            client_id=CLIENT_ID,
            client_secret=CLIENT_SECRET,
            use_cache=True,
            bearer_token=self.bearer_token
        )

    def user(self, username):
        user = self.twitch.user(username)
        return user

    def channel(self, channel_id):
        url = 'https://api.twitch.tv/helix/channels?broadcaster_id=' + channel_id
        headers = self.get_auth_headers()
        r = requests.get(url, headers=headers).json()
        return r

    def stream(self, user_id):
        url = 'https://api.twitch.tv/helix/streams?user_id=' + user_id
        headers = self.get_auth_headers()
        r = requests.get(url, headers=headers).json()
        return r

    def user_follows(self, user: User):
        base_url = f"https://api.twitch.tv/helix/users/follows?to_id={CHAN_ID}&from_id={user.id}"
        headers = self.get_auth_headers()
        follows = requests.get(base_url, headers=headers)
        follows = json.loads(follows.text)
        return follows

    def get_auth_headers(self):
        return {'Client-ID': CLIENT_ID, 'Authorization': self.bearer_token}
