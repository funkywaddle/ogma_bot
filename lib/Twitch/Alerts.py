from pprint import pprint
import asyncio, ssl, requests
import json
from datetime import datetime, timedelta
from config import CLIENT_ID, CLIENT_SECRET
from models.Config import Config
from lib.Twitch.Auth import Auth
import lib.vendor.websockets as websockets

# !add command [command] [response]
# !add command qc He is the man



class Alerts:
    def __init__(self):
        super().__init__()
        self.auth = Auth()
        self.connected = False
        self.host = "wss://pubsub-edge.twitch.tv"
        self.port = 443
        self.events = []
        self.last_ping = None
        self.channel = None
        self.access_token = 'Bearer ' + self.auth.get_bearer_token()
        self.refresh_token = None
        self.auth_expires = None
        self.token_type = None

    def refresh_auth(self):
        pass

    def add_listener(self, event):
        self.events.append(event)

    async def send_message(self, msg):
        # print(f'sending: {msg}')
        # if datetime.now() > self.auth_expires:
        #     self.get_auth()

        async with websockets.connect(self.host) as websocket:
            await websocket.send(msg)
            return await websocket.recv()

    def send_listen_requests(self):
        data = dict(topics=self.events, auth_token=CLIENT_SECRET)
        events = dict(type='LISTEN', data=data)
        pprint(events)
        return asyncio.run(self.send_message(json.dumps(events)))
        # self.send_message(data)

    # def get_message(self):
    #     msg = self.recv(1024).decode("utf-8")
    #     # print(f'received: {msg}')
    #     return msg

    def send_ping(self, ws):
        data = json.dumps({"type": "PING"})
        self.last_ping = datetime.now()
        response = ws.send(data=data)
        pprint(response)
        pong = json.dumps(response)
        if pong != '{"type": "PONG"}':
            self.last_ping = None
            # self.start()
