import re
from models.Config import Config

try:
    NICK = Config.get_config('BOT_NICK')
    PASS = Config.get_config('OAUTH')
    CHAN = '#'+Config.get_config('CHANNEL')
    CHAN_ID = Config.get_config('CHANNEL_ID')
    CLIENT_ID = Config.get_config('CLIENT_ID')
    CASTER = Config.get_config('CASTER_NICK')
    CHANNEL_LANGUAGE = Config.get_config('CHANNEL_LANGUAGE')
    CLIENT_SECRET = Config.get_config('CLIENT_SECRET')
except Exception as e:
    print(str(e))

RATE = (20/30)
CHAT_MSG = r"(.*):(\w+)!\w+@\w+\.tmi\.twitch\.tv PRIVMSG #\w+ :(.*)"
